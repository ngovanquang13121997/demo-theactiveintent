import microphone_image from "./assets/microphone.js";
import right_svg from "./assets/right_svg.js";
import spaeaker_image from "./assets/speaker";

var stringSimilarity = require("string-similarity");

let buttonStyle =
  "style='padding: 0;border: none;background: none;cursor: pointer;'";

let textVoice = "";

let textVoicePermission =
  "Enable permissions on the upper left of your browser window";

let textListen = "<div class='the_activeintent_loading'>Listening </div>";

let status = 0;

class App {
  activationKey = undefined;

  constructor(intents) {
    this.intents = intents;
    if (window.hasOwnProperty("webkitSpeechRecognition")) {
      this.recognition = new window.webkitSpeechRecognition();
      this.recognition.lang = "en-US";
      this.recognition.interimResults = false;
      this.recognition.continuous = false;
    }

    let link = document.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('href', 'https://cdn.jsdelivr.net/npm/active-intent@latest/activeintent.css');
    document.head.appendChild(link);

    if (window.hasOwnProperty("webkitSpeechGrammarList")) {
      this.speechRecognitionList = new window.webkitSpeechGrammarList();
      this.speechRecognitionList.addFromString("#JSGF V1.0;", 1);
      this.recognition.grammars = this.speechRecognitionList;
    }

    const script = window.document.getElementById("ActiveIntent");
    if (script) {
      this.activationKey = script.getAttribute("activationKey");
    }
    if (!this.activationKey) return;
    this.usingSDKRequest();
  }

  usingSDKRequest = () => {
    let hostname = window.location.hostname;
    // let hostname = "theactiveintent.com";

    let xmlhttp = new XMLHttpRequest();

    if (this.recognition) {
      this.configureRecognition();
      this.addMicrophoneElement();
    }
    this.addSpeakerElements();
    this.addListener();

    xmlhttp.open(
      "GET",
      `https://theactiveintent.com/api/v1/customer/using?hostname=${hostname}&activationKey=${this.activationKey}`,
      true
    );
    xmlhttp.send();
  };

  findTheIntent = (phrase) => {
    for (let intent of this.intents) {
      if (intent.phrases.includes(phrase)) {
        return intent;
      }
      for (let phrase_source of intent.phrases) {
        if (phrase.startsWith(phrase_source)) {
          return intent;
        }
      }
    }
  };

  configureRecognition = () => {
    let findTheIntent = this.findTheIntent;
    let previousIntent;

    this.recognition.onresult = function (event) {
      status = 0;
      var lastResult = event.results.length - 1;
      var content = event.results[lastResult][0].transcript.trim();
      let intent = findTheIntent(content);

      console.log({ lastResult, content }, "content");

      let message = "";
      if (intent !== undefined) {
        previousIntent = intent;

        if (intent.name === "ArticleIntent") {
          let elements = document.querySelectorAll("article h1");
          if (elements.length === 0) {
            message = "There are no articles";
          } else {
            message = `There are ${elements.length} articles. Choose any one.\r\n`;
            elements.forEach(function (element, i) {
              message += `${i + 1} ${element.textContent.trim()}\r\n`;
            });
          }
        } else {
          let element = document.querySelector(
            "[og-intent='" + intent.name + "']"
          );
          if (element !== null && element !== undefined) {
            message = element.textContent.trim();
          }
        }
      } else if (
        intent === undefined &&
        previousIntent !== undefined &&
        previousIntent.name === "ArticleIntent"
      ) {
        // var similarity = stringSimilarity.compareTwoStrings("healed", "sealed");
        // console.log(similarity);

        let elements = document.querySelectorAll("article h1");
        let list_of_articles = [];
        elements.forEach((element) => {
          list_of_articles.push(element.textContent.trim());
        });
        var matches = stringSimilarity.findBestMatch(content, list_of_articles);
        if (matches.bestMatchIndex !== 0) {
          message = document
            .querySelectorAll("article p")
            [matches.bestMatchIndex].textContent.trim();
        } else {
          message = `No Articles found with ${content}`;
        }
      } else {
        message = "We haven't set it up";
      }

      if (
        intent !== undefined &&
        intent.type !== undefined &&
        intent.type === "action"
      ) {
        for (let phrase of intent.phrases) {
          content = content.replace(phrase, "");
        }
        let elements = document.querySelectorAll(
          "[og-intent='" + intent.name + "']"
        );
        for (let element of elements) {
          if (
            content.trim().toLowerCase() ===
            element.textContent.trim().toLowerCase()
          ) {
            element.click();
          }
        }
      } else {
        speechSynthesis.cancel();
        speechSynthesis.speak(new SpeechSynthesisUtterance(message));
      }
    };

    this.recognition.onspeechend = function () {
      let classPermission = document.getElementsByClassName("the_activeintent_voice_flex");
      classPermission[0].className += " the_activeintent_display_none";

      let classFlex = document.querySelectorAll(
        ".the_activeintent_voice_flex.the_activeintent_top_60.the_activeintent_voice_flex_top_0"
      );
      if (classFlex.length > 0) {
        classFlex[0].classList.remove("the_activeintent_voice_flex_top_0");
      }

      let classSvg = document.querySelectorAll(".the_activeintent_right_svg.the_activeintent_right_svg_none");
      if (classSvg.length > 0) {
        classSvg[0].classList.remove("the_activeintent_right_svg_none");
      }

      let classAvatar = document.querySelectorAll(".the_activeintent_avatar.the_activeintent_avatar_voice");
      if (classAvatar.length > 0) {
        classAvatar[0].classList.remove("the_activeintent_avatar_voice");
      }

      status = 0;
    };

    this.recognition.onerror = function (event) {
      if (event.error === "not-allowed") {
        let classPermission = document.getElementsByClassName("the_activeintent_voice_flex");
        classPermission[0].className += " the_activeintent_display_none";
      }

      if (event.error === "no-speech") {
        let classPermission = document.getElementsByClassName("the_activeintent_voice_flex");
        classPermission[0].className += " the_activeintent_display_none";

        let classFlex = document.querySelectorAll(
          ".the_activeintent_voice_flex.the_activeintent_top_60.the_activeintent_voice_flex_top_0"
        );
        if (classFlex.length > 0) {
          classFlex[0].classList.remove("the_activeintent_voice_flex_top_0");
        }

        let classSvg = document.querySelectorAll(".the_activeintent_right_svg.the_activeintent_right_svg_none");
        if (classSvg.length > 0) {
          classSvg[0].classList.remove("the_activeintent_right_svg_none");
        }

        let classAvatar = document.querySelectorAll(".the_activeintent_avatar.the_activeintent_avatar_voice");
        if (classAvatar.length > 0) {
          classAvatar[0].classList.remove("the_activeintent_avatar_voice");
        }
      }

      status = 0;
      // console.log(event.error,'ERROR');
    };
  };
  addMicrophoneElement = () => {
    let microphoneElement = document.createElement("div");

    let htmlVoice =
      '<div class="the_activeintent" style="position: fixed; top: 85%; right: 5%">' +
      '    <button ' +
      buttonStyle +
      ">" +
      '      <div style="display: flex">' +
      '        <div class="the_activeintent_voice_flex the_activeintent_display_none the_activeintent_top_60">' +
      '<span id="the_activeintent_text_voice">' +
      textVoice +
      "</span>" +
      '          <div class="the_activeintent_right_svg" style="position: absolute;right: -25px;bottom: -5px;">' +
      right_svg +
      "</div>" +
      "        </div>" +
      '        <div id="microphone" class="the_activeintent_avatar">' +
      microphone_image +
      "</div>" +
      "      </div>" +
      "    </button>" +
      "  </div>";

    microphoneElement.innerHTML = htmlVoice;
    document.body.appendChild(microphoneElement);
  };

  addSpeakerElements = () => {
    for (let element of document.querySelectorAll("[magic='speakable']")) {
      let miniMicrophoneElement = document.createElement("div");
      miniMicrophoneElement.innerHTML =
        "<div  ><button class='speaker' " +
        buttonStyle +
        " ><img style='width: 25px;' src='" +
        spaeaker_image +
        "'/> </button></div>";
      element.appendChild(miniMicrophoneElement);
    }
  };

  addListener = () => {
    let recognition = this.recognition;

    if (this.recognition) {
      document
        .getElementById("microphone")
        .addEventListener("click", function () {
          if (status === 1) {
            status = 0;
            return recognition.stop();
          }

          let classDisplayBlock = document.querySelectorAll(
            ".the_activeintent_voice_flex.the_activeintent_display_none"
          );
          if (classDisplayBlock.length > 0) {
            classDisplayBlock[0].classList.remove("the_activeintent_display_none");
          }

          let x = document.getElementById("the_activeintent_text_voice");
          x.innerHTML = textVoicePermission;

          recognition.start();
          status = 1;
          recognition.onstart = function () {
            let classVoiceFlex = document.getElementsByClassName("the_activeintent_voice_flex");
            if (classVoiceFlex.length > 0) {
              classVoiceFlex[0].className += " the_activeintent_voice_flex_top_0";
            }

            let classRightSvg = document.getElementsByClassName("the_activeintent_right_svg");
            if (classRightSvg.length > 0) {
              classRightSvg[0].className += " the_activeintent_right_svg_none";
            }

            let classAvatar = document.getElementsByClassName("the_activeintent_avatar");
            if (classAvatar.length > 0) {
              classAvatar[0].className += " the_activeintent_avatar_voice";
            }

            let x = document.getElementById("the_activeintent_text_voice");
            x.innerHTML = textListen;
          };
        });
    }

    setInterval(() => {
      let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

      let isChrome = /Chrome/i.test(navigator.userAgent);
      if (!isMobile && isChrome) {
        speechSynthesis.pause();
      }
      speechSynthesis.resume();
    }, 10000); //Fix https://stackoverflow.com/questions/21947730/chrome-speech-synthesis-with-longer-texts

    for (let element of document.querySelectorAll(".speaker")) {
      element.addEventListener("click", function () {
        speechSynthesis.cancel();
        let message =
          this.parentElement.parentElement.parentElement.textContent.trim();
        // console.log(message);
        speechSynthesis.speak(new SpeechSynthesisUtterance(message));
      });
    }
  };
}

export default App;
